package cz.vutbr.web.csskit.antlr4;

import org.antlr.v4.runtime.tree.*;

public class CSSParserFullVisitor<T> extends AbstractParseTreeVisitor<T> implements CSSParserVisitor<T> {

    @Override
    public T visitInlinestyle(CSSParser.InlinestyleContext ctx) {
        return null;
    }

    @Override
    public T visitStylesheet(CSSParser.StylesheetContext ctx) {
        return null;
    }

    @Override
    public T visitStatement(CSSParser.StatementContext ctx) {
        return null;
    }

    @Override
    public T visitRuleset(CSSParser.RulesetContext ctx) {
        return null;
    }

    @Override
    public T visitDeclarations(CSSParser.DeclarationsContext ctx) {
        return null;
    }

    @Override
    public T visitDeclaration(CSSParser.DeclarationContext ctx) {
        return null;
    }

    @Override
    public T visitImportant(CSSParser.ImportantContext ctx) {
        return null;
    }

    @Override
    public T visitProperty(CSSParser.PropertyContext ctx) {
        return null;
    }

    @Override
    public T visitTerms(CSSParser.TermsContext ctx) {
        return null;
    }

    @Override
    public T visitTermValuePart(CSSParser.TermValuePartContext ctx) {
        return null;
    }

    @Override
    public T visitTermCurlyBlock(CSSParser.TermCurlyBlockContext ctx) {
        return null;
    }

    @Override
    public T visitTermAtKeyword(CSSParser.TermAtKeywordContext ctx) {
        return null;
    }


    @Override
    public T visitFunct(CSSParser.FunctContext ctx) {
        return null;
    }

    @Override
    public T visitValuepart(CSSParser.ValuepartContext ctx) {
        return null;
    }

    @Override
    public T visitCombined_selector(CSSParser.Combined_selectorContext ctx) {
        return null;
    }

    @Override
    public T visitCombinatorChild(CSSParser.CombinatorChildContext ctx) {
        return null;
    }

    @Override
    public T visitCombinatorAdjacent(CSSParser.CombinatorAdjacentContext ctx) {
        return null;
    }

    @Override
    public T visitCombinatorPreceding(CSSParser.CombinatorPrecedingContext ctx) {
        return null;
    }

    @Override
    public T visitCombinatorDescendant(CSSParser.CombinatorDescendantContext ctx) {
        return null;
    }


    @Override
    public T visitSelectorWithIdOrAsterisk(CSSParser.SelectorWithIdOrAsteriskContext ctx) {
        return null;
    }

    @Override
    public T visitSelectorWithoutIdOrAsterisk(CSSParser.SelectorWithoutIdOrAsteriskContext ctx) {
        return null;
    }

    @Override
    public T visitSelpartId(CSSParser.SelpartIdContext ctx) {
        return null;
    }

    @Override
    public T visitSelpartClass(CSSParser.SelpartClassContext ctx) {
        return null;
    }

    @Override
    public T visitSelpartAttrib(CSSParser.SelpartAttribContext ctx) {
        return null;
    }

    @Override
    public T visitSelpartPseudo(CSSParser.SelpartPseudoContext ctx) {
        return null;
    }

    @Override
    public T visitSelpartInvalid(CSSParser.SelpartInvalidContext ctx) {
        return null;
    }


    @Override
    public T visitAttribute(CSSParser.AttributeContext ctx) {
        return null;
    }

    @Override
    public T visitPseudo(CSSParser.PseudoContext ctx) {
        return null;
    }

    @Override
    public T visitPseudocolon(CSSParser.PseudocolonContext ctx) {
        return null;
    }

    @Override
    public T visitString(CSSParser.StringContext ctx) {
        return null;
    }

    @Override
    public T visitAny(CSSParser.AnyContext ctx) {
        return null;
    }

    @Override
    public T visitNostatement(CSSParser.NostatementContext ctx) {
        return null;
    }

    @Override
    public T visitNoprop(CSSParser.NopropContext ctx) {
        return null;
    }

    @Override
    public T visitNorule(CSSParser.NoruleContext ctx) {
        return null;
    }

    @Override
    public T visitNomediaquery(CSSParser.NomediaqueryContext ctx) {
        return null;
    }

    @Override
    public T visitAtstatement(CSSParser.AtstatementContext ctx) {
        return null;
    }

    @Override
    public T visitImport_uri(CSSParser.Import_uriContext ctx) {
        return null;
    }

    @Override
    public T visitPage(CSSParser.PageContext ctx) {
        return null;
    }

    @Override
    public T visitPage_pseudo(CSSParser.Page_pseudoContext ctx) {
        return null;
    }

    @Override
    public T visitMargin_rule(CSSParser.Margin_ruleContext ctx) {
        return null;
    }

    @Override
    public T visitInlineset(CSSParser.InlinesetContext ctx) {
        return null;
    }

    @Override
    public T visitMedia(CSSParser.MediaContext ctx) {
        return null;
    }

    @Override
    public T visitMedia_query(CSSParser.Media_queryContext ctx) {
        return null;
    }

    @Override
    public T visitMedia_term(CSSParser.Media_termContext ctx) {
        return null;
    }

    @Override
    public T visitMedia_expression(CSSParser.Media_expressionContext ctx) {
        return null;
    }

    @Override
    public T visitMedia_rule(CSSParser.Media_ruleContext ctx) {
        return null;
    }

    @Override
    public T visitUnknown_atrule(CSSParser.Unknown_atruleContext ctx) {
        return null;
    }
}
