import cz.vutbr.web.csskit.DefaultNetworkProcessor;
import cz.vutbr.web.csskit.antlr4.*;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;
import java.net.URL;

public class Main {
    public static void main(String args[]) {
        URL u = Main.class.getResource("/input.css");
//        CSSParserFactory pf = CSSParserFactory.getInstance();
//        pf.parse()
        CSSInputStream is = null;
        try {
            is = CSSInputStream.urlStream(u, new DefaultNetworkProcessor(), null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        CSSLexer lexer = new CSSLexer(is);
        lexer.init();
        lexer.addErrorListener(new CSSLexerErrorListener());

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CSSParser parser = new CSSParser(tokens);

        parser.addErrorListener(new CSSParserErrorListener());


        //toto uz je v parse
        ParserRuleContext tree = parser.stylesheet(); // parse
        ParseTreeWalker walker = new ParseTreeWalker(); // create standard walker
        CSSParserListenerImpl extractor = new CSSParserListenerImpl(null, null);

        walker.walk(extractor, tree); // initiate walk of tree with listener

        /*
        extractor.printStmtList();
        Generator g = new Generator(extractor.getStatements(), outputFileName);
        g.generate();
        System.exit(State.RETURN_STATE_OK);
        */
    }
}
