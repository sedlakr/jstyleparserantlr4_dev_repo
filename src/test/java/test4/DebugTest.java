package test4;

import cz.vutbr.web.css.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class DebugTest {
    private static StyleSheet sheet;
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(DebugTest.class);

    @BeforeClass
    public static void init() throws IOException, CSSException {
//        sheet = CSSFactory.parse(DebugTest.class.getResource("/antlr4/test.css"), null);
//        sheet = CSSFactory.parseString("body{color:red}", null);
//        log.info("init");
    }

    @Test
    public void testA() {
        Assert.assertNotNull(sheet);
    }

    @Test
    public void testB() {
        log.info("testb");
    }

    @Test
    public void testUnknownAtRule() throws IOException, CSSException {
        String s = "@three-dee {@background-lighting {azimuth: 30deg; elevation: 190deg; } h2 { color: green; text-decoration: blink } } h2 {color: blue}";
        System.out.println(s);
        StyleSheet sheet = CSSFactory.parseString(s, null);
        return;
    }

    @Test
    public void testMalformedDeclaration() throws IOException, CSSException {
        String s = "p { color:green } ";
        String s1 = "p { color:green; color } ";
        String s2 = "p { color:red;   color; color:green } ";
        String s3 = "p { color:green; color: } ";
        String s4 = "p { color:red;   color:; color:green } ";
        String s5 = "p { color:green; color{;color:maroon} } ";
        String s6 = "p { color:red;   color{;color:maroon}; color:green }";
        TermColor c = CSSFactory.getTermFactory().createColor(0, 0x80, 0);
        StyleSheet sheet = CSSFactory.parseString(s6, null);
        Declaration d = ((RuleSet) sheet.get(0)).get(1);
        TermColor cc = (TermColor) d.get(0);
        Assert.assertEquals("Color is green", cc.getValue(), c.getValue());
        return;
    }
    @Test
    public void unclosedString() throws IOException, CSSException {
        String s = "@media screen {p:before { content: 'Hello";
        StyleSheet sheet = CSSFactory.parseString(s, null);
    }


}
