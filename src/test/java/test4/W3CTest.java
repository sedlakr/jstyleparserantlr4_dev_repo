package test4;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.vutbr.web.css.CSSException;
import cz.vutbr.web.css.CSSFactory;
import cz.vutbr.web.css.Declaration;
import cz.vutbr.web.css.RuleSet;
import cz.vutbr.web.css.StyleSheet;
import cz.vutbr.web.css.TermFactory;
import cz.vutbr.web.css.TermInteger;
import cz.vutbr.web.css.TermLength;
import cz.vutbr.web.css.TermNumeric.Unit;
import cz.vutbr.web.css.TermURI;
import test.DeclarationsUtil;
import test.SelectorsUtil;

public class W3CTest {
    private static final Logger log = LoggerFactory.getLogger(W3CTest.class);

    public static final TermFactory tf = CSSFactory.getTermFactory();

    public static final String TEST_STRING1 =
            "h1 { color: red; rotation: 70minutes }";

    @BeforeClass
    public static void init()  {
        log.info("\n\n\n == W3CTest test at {} == \n\n\n", new Date());
    }
    @Test
    public void testUnknownProperty() throws IOException, CSSException   {
//
//        StyleSheet ss;
//
//        ss = CSSFactory.parseString(TEST_STRING1, null);
//
//        assertEquals("No rules are defined", 0, ss.size());

/*        ss = CSSFactory.parseString(TEST_CHARSET_STRING2, null);

        assertEquals("One rule is set", 1, ss.size());

        RuleSet rule = (RuleSet) ss.get(0);

        assertArrayEquals("Rule contains one selector BODY ",
                SelectorsUtil.createSelectors("BODY"),
                rule.getSelectors());

        assertEquals("Rule contains one declaration { color: blue;}",
                DeclarationsUtil.appendDeclaration(null, "color",
                        tf.createColor(0,0,255)),
                rule.asList());*/
    }
}