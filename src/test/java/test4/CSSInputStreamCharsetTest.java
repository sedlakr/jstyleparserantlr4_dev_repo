package test4;

import java.io.IOException;
import java.net.URL;
import java.util.Date;

import cz.vutbr.web.css.*;
import cz.vutbr.web.csskit.DefaultNetworkProcessor;
import cz.vutbr.web.csskit.antlr4.CSSInputStream;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import cz.vutbr.web.css.CSSProperty.FontFamily;
import cz.vutbr.web.css.Term.Operator;
import cz.vutbr.web.domassign.Analyzer;
import cz.vutbr.web.domassign.StyleMap;

public class CSSInputStreamCharsetTest {

    private static final Logger log = LoggerFactory.getLogger(CSSInputStreamCharsetTest.class);

//    private static TermFactory tf = CSSFactory.getTermFactory();

//	private static Document doc;
//	private static StyleSheet sheet;
//	private static Analyzer analyzer;
//	private static ElementMap elements;
//	private static StyleMap decl;

    @BeforeClass
    public static void init() throws IOException, CSSException, SAXException {


    }

    @Test
    public void testCharset() throws IOException, CSSException, SAXException {

        log.info("\n\n\n == CSSInputStreamCharsetTest test at {} == \n\n\n", new Date());
        URL u;
        CSSInputStream is;


        u = CSSInputStreamCharsetTest.class.getResource("/antlr4/windows-1250.css");
        is = CSSInputStream.urlStream(u, new DefaultNetworkProcessor(), null);
        is.setEncoding("windows-1250");
        System.out.println(is.getEncoding());
        Assert.assertEquals("windows-1250",is.getEncoding());

        u = CSSInputStreamCharsetTest.class.getResource("/antlr4/utf-8.css");
        is = CSSInputStream.urlStream(u, new DefaultNetworkProcessor(), null);
        Assert.assertEquals(is.getEncoding(), "UTF-8");
//        is.setEncoding("UTF-8");
//        System.out.println(is.getEncoding());

//        DOMSource ds = new DOMSource(CSSInputStreamCharsetTest.class.getResourceAsStream("/advanced/style.html"));
//        doc = ds.parse();
//
//		sheet = CSSFactory.parse(CSSInputStreamCharsetTest.class.getResource("/advanced/style.css"), null);
//
//		analyzer = new Analyzer(sheet);
//		decl = analyzer.evaluateDOM(doc, "all", true);
//
//		elements = new ElementMap(doc);
    }

}
